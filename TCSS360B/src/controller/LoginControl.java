/**
 * 
 */

package controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import model.User;

/**
 * @author Ryan Jeffries
 *
 */
public class LoginControl {
	
	private PortalControl my_portal;
	
	public LoginControl(PortalControl the_portalC) {
		my_portal = the_portalC;
	}
	
	public boolean login(String the_email, String the_password) {
		boolean isValidUser = false;
		Connection con = DBConnector.connect();
		try {
			Statement statement = con.createStatement();	
			ResultSet rs = statement.executeQuery("SELECT * FROM userpasswords WHERE userpasswords_user_email=\'" + the_email + "\';");
			if (rs.next()) {
				String myPassword = rs.getString("userpasswords_password");
				if (the_password.equals(myPassword)) {
					isValidUser = true;
				}
			}
			if (isValidUser) {
				statement = con.createStatement();	
				rs = statement.executeQuery("SELECT * FROM users WHERE users_email=\'" + the_email + "\';");
				User account = new User(rs.getInt("users_id"), rs.getString("users_first"), rs.getString("users_last"), rs.getString("users_email"));
				int a = rs.getInt("users_isAdmin");
				boolean isAdmin = false;
				if (a == 1) {
					isAdmin = true;
				}
				my_portal.logIn(account, isAdmin);		
			}
			DBConnector.closeConnection(con);
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}		
		return isValidUser;

	}
	
	public boolean register(String the_first, String the_last, String the_email, String the_email_confir, String the_password, String the_password_confir) {
	     
		boolean successful = false;
		
		// Bug in if statement. Checking the email and password confirmations
		// was returning the wrong result. Fixed by Zach
		if (the_first != null && the_last != null && the_email.equals(the_email_confir) && the_password.equals(the_password_confir)) {
			Connection con = DBConnector.connect();
			try {
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery("SELECT users_email FROM users WHERE users_email=\'" + the_email + "\';");
				if (!rs.next()) {
					statement.execute("INSERT INTO users (users_first, users_last, users_email) VALUES (\'" + the_first + "\',\'" + the_last + "\',\'" + the_email + "\');");
					statement.execute("INSERT INTO userpasswords (userpasswords_user_email, userpasswords_password) VALUES (\'" + the_email + "\',\'" + the_password + "\');");
					successful = true;
				}
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}	
			DBConnector.closeConnection(con);
		}
		return successful;
	}
	
	public User getUser(String the_email) {
		User returnedUser = null;
		Connection con = DBConnector.connect();
		try {
			Statement statement = con.createStatement();	
			ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE users_email=\'" + the_email + "\';");
			returnedUser = new User(rs.getInt("users_id"), rs.getString("users_first"), rs.getString("users_last"), rs.getString("users_email"));
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}		
		DBConnector.closeConnection(con);
		return returnedUser;
	}
	
	/**
	 * 
	 * 
	 * @param the_email The email to login with.
	 * @param the_password The password to login with which corresponds to the email.
	 * @return GenericUser of account else null if could not login.
	 */
	public boolean checkPassword(String the_email, String the_password) {
		boolean isValidUser = false;
		Connection con = DBConnector.connect();
		try {
			Statement statement = con.createStatement();	
			ResultSet rs = statement.executeQuery("SELECT * FROM userpasswords WHERE userpasswords_user_email=\'" + the_email + "\';");
			if (rs.next()) {
				String myPassword = rs.getString("userpasswords_password");
				if (the_password.equals(myPassword)) {
					isValidUser = true;
				}
			}
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}
		DBConnector.closeConnection(con);
		return isValidUser;
	}
}
