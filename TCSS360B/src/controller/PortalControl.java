/**
 * 
 */

package controller;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import model.Conference;
import model.Paper;
import model.Recommendation;
import model.Review;
import model.User;

/**
 * @author Ryan Jeffries
 *
 */
public class PortalControl extends Observable {
	
	private User my_logged_in_user;

	private boolean logged_in;
	
	private List<Conference> my_conferences;
	
	private List<Paper> my_papers;
	
	private Paper my_paper;
	
	private Conference my_conference;
	
	private List<Conference> all_conferences;
	
	private List<User> all_users;
	
	private int my_user_paper_role;
	
	private List<Review> my_reviews;
	
	private List<Recommendation> my_recommendations;

	private boolean isadmin;
	
	private List<User> my_reviewers;
	
	private User my_subprogramchair;
	
	private User my_programchair;
	
	private User my_author;
	
	private String my_paper_role;
	
	public PortalControl() {
		logged_in = false;
		isadmin = false;
		my_conferences = new ArrayList<Conference>();
		my_papers = new ArrayList<Paper>();
		all_conferences = new ArrayList<Conference>();
		all_users = new ArrayList<User>();
		my_reviews = new ArrayList<Review>();
		my_recommendations = new ArrayList<Recommendation>();
		my_reviewers = new ArrayList<User>();
	}
	
	public void logIn(User user, boolean is_admin) {	
		my_logged_in_user = user;
		logged_in = true;
		isadmin = is_admin;
		updateAllUsers();
		updateConferences();
		setChanged();
		notifyObservers("LgI");
	}
	
	public void logOut() {
		isadmin = false;
		my_logged_in_user = null;
		logged_in = false;
		my_conferences.clear();
		my_conference = null;
		my_papers.clear();
		my_paper = null;
		my_reviews.clear();
		setChanged();
		notifyObservers("LgO");
	}
	
	public boolean isAdmin() {
		return isadmin;
	}
	
	public User getUser() {
		return my_logged_in_user;
	}
	
	public void update() {
		notifyObservers();
	}
	
	public boolean isLoggedIn() {
		return logged_in;
	}
	
	public boolean haveConferences() {
		return logged_in;
	}
	
	private void updateAllUsers() {
		Connection con = DBConnector.connect();
		try {
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM users");
			while (rs.next()) {
				User user = new User(rs.getInt("users_id"), rs.getString("users_first"), rs.getString("users_last"), rs.getString("users_email"));
				all_users.add(user);
			}
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}
		DBConnector.closeConnection(con);
	}
	
	public List<User> getAllUsers() {
		return all_users;
	}
	
	private User getUser(int user_id) {
		for (User u : all_users) {
			if (u.getID() == user_id) {
				return u;
			}
		}
		return null;
	}	
	
	//
	//
	//
	// Conference Controllers
	//	
	//
	//
	
	/**
	 * Helper method to populate all_conferences and my_conferences at login.
	 */
	private void updateConferences() {
		Connection con = DBConnector.connect();
		try {
			if (all_conferences.isEmpty()) {
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery("SELECT * FROM conferences;");
				while (rs.next()) {
					int programchairID = rs.getInt("conferences_programchair_id");	
					Conference cf = new Conference(rs.getInt("conferences_id"), rs.getString("conferences_name"), rs.getString("conferences_description"), rs.getString("conferneces_paperdeadline"), rs.getString("conferences_date"), rs.getString("conferences_location"), getUser(programchairID));
					all_conferences.add(cf);
				}
			}
			Statement statement1 = con.createStatement();
			ResultSet rs1 = statement1.executeQuery("SELECT * FROM user_conference_maps WHERE userconference_user_id=\'" + my_logged_in_user.getID() + "\';");
			while (rs1.next()) {
				for (Conference c : all_conferences) {
					if (c.getID() == rs1.getInt("userconference_conference_id")) {
						my_conferences.add(c);
					}
				}
			}	
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}	
		DBConnector.closeConnection(con);

	}
	
	/**
	 * Adds a conference to all_conferences.
	 * 
	 * @param the_name Name of the conference.
	 * @param the_description Description of the conference.
	 * @param the_paperdeadline Deadline of papers to be turned into the conference.
	 * @param the_date Date of the conference to be held.
	 * @param the_location Location of the conference.
	 */
	public boolean addConference(String the_name, String the_description, String the_paperdeadline, String the_date, String the_location, User the_programchair) {
		boolean isSuccessful = false;
		if (the_name != null && the_description != null && the_paperdeadline != null && the_date != null && the_location != null && the_programchair != null) {
	 		Conference cf = new Conference(the_name, the_description, the_date, the_paperdeadline, the_location, the_programchair);
			Connection con = DBConnector.connect();
			try {
				Statement statement = con.createStatement();
				statement.execute("INSERT INTO conferences (conferences_name, conferences_description, conferneces_paperdeadline, conferences_date, conferences_location, conferences_programchair_id) VALUES (\'" + cf.getName() + "\',\'" + cf.getDescription() + "\',\'" + cf.getPaperdeadline() + "\',\'" + cf.getDate() + "\',\'" + cf.getLocation() + "\',\'" + the_programchair.getID() + "\');");
				statement = con.createStatement();
				ResultSet rs = statement.executeQuery("SELECT conferences_id FROM conferences WHERE conferences_name=\'" + cf.getName() + "\' AND conferences_date=\'" + cf.getDate() + "\';");
				cf.setID(rs.getInt("conferences_id"));
				all_conferences.add(cf);
				setChanged();
				notifyObservers("NewCon");
				isSuccessful = true;
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}
			DBConnector.closeConnection(con);	
		}
		return isSuccessful;
	}
	
	/**
	 * Returns a list of all created conferences regardless of user logged in.
	 * @return
	 */
	public List<Conference> getAllConferences() {
		return all_conferences;
	}
	
	/**
	 * Returns current conference.
	 * 
	 * @return Current conference.
	 */
	public Conference getConference() {
		return my_conference;
	}
	
	/**
	 * Returns a conference based on the user logged in and the provided conference id.
	 * 
	 * @param the_conference The conference to search.
	 */
	public Conference getConference(int the_conference_id) {
		Conference theConference = null;
		for (Conference cf : my_conferences) {
			if (cf.getID() == the_conference_id) {
				theConference = cf;
				break;
			}
		}
		return theConference;
	}

	/**
	 * Returns all conferences current user is apart of.
	 * 
	 * @return Associated conference the current user knows about.
	 */
	public List<Conference> getConferences() {
		return my_conferences;
	}
	
	/**
	 * Returns number of conferences known by the user.
	 * 
	 * @return Number of conferences known by the user.
	 */
	public int conferencesSize() {
		return my_conferences.size();
	}
	
	/**
	 * Selects the conference which is considered current.
	 * 
	 * @param the_conferenceId Id of the conference to select as current.
	 */
	public void selectConference(int the_conferenceId) {
		my_papers.clear();
		for (Conference cf : my_conferences) {
			if (cf.getID() == the_conferenceId) {
				my_conference = cf;
				my_programchair = cf.getProgramchair();
				updatePapers();
				setChanged();
				notifyObservers(my_conference);
				break;
			}
		}
	}
	
	/**
	 * 
	 * @param the_conference
	 */
	public void addConferenceToKnown(Conference the_conference) {
		if (the_conference != null && !my_conferences.contains(the_conference)) {
			my_conferences.add(the_conference);
		}
	}
	
	//
	//
	//
	// Paper Controllers
	//	
	//
	//
	
	/**
	 * Helper method to update papers based off of my_conference.
	 */
	private void updatePapers() {
		Connection con = DBConnector.connect();
		try {
			Statement statement1 = con.createStatement();
			ResultSet rs1;
			String theRole = "";
			if (my_programchair.getID() == my_logged_in_user.getID()) {
				theRole = "Program Chair";
				rs1 = statement1.executeQuery("SELECT * FROM papers WHERE papers_conference_id_fk=\'" + my_conference.getID() + "\';");
			} else {
				rs1 = statement1.executeQuery("SELECT * FROM papers WHERE papers_conference_id_fk=\'" + my_conference.getID() + "\' AND (papers_author=\'" + my_logged_in_user.getID() + "\' OR papers_subprogramchair=\'" + my_logged_in_user.getID() + "\');");	
			}
			while (rs1.next()) {
				User Author = getUser(rs1.getInt("papers_author"));
				User Subprogram = getUser(rs1.getInt("papers_subprogramchair"));
				if (my_programchair.getID() == my_logged_in_user.getID()) {
					theRole = "Program Chair";
				} else if (Author.getID() == my_logged_in_user.getID()) {
					theRole = "Author";
				} else if (Subprogram != null && Subprogram.getID() == my_logged_in_user.getID()) {
					theRole = "Subprogram Chair";
				}
				Paper paper = new Paper(rs1.getString("papers_title"), rs1.getString("papers_abstract"), rs1.getString("papers_file"), rs1.getString("papers_status"), Author, Subprogram, new ArrayList<User>(), theRole);
				paper.setID(rs1.getInt("papers_id"));
				my_papers.add(paper);
			}
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}
		DBConnector.closeConnection(con);
	}
	
	/**
	 * Adds a paper to the selected conference.
	 * 
	 * @param the_title Title of the paper.
	 * @param the_abstract Abstract of the paper.
	 * @param the_file File of the paper.
	 */
	public void addPaper(String the_title, String the_abstract, String the_text) {
		if (my_conference != null && my_logged_in_user != null) {	
			Connection con = DBConnector.connect();
			String theRole = "Author";
			String subStatus = "undecided)";
			Paper paper = new Paper(the_title, the_abstract, the_text, subStatus, my_logged_in_user, null, new ArrayList<User>(), theRole);
			try {
				//Insert paper into database
				Statement statement = con.createStatement();
				statement.execute("INSERT INTO papers (papers_conference_id_fk, papers_title, papers_file, papers_datesubmitted, papers_status, papers_abstract, papers_author, papers_subprogramchair, papers_file) VALUES (\'" + my_conference.getID() + "\',\'" + paper.getTitle() + "\',\'" + paper.getFile() + "\',\'" + paper.getDate() + "\',\'" + subStatus + "\',\'" + paper.getAbstract() + "\',\'" + my_logged_in_user.getID() + "\',\'"  + -1 + "\',\'" + the_text + "\');");
				statement = con.createStatement();
				//Update papers id
				ResultSet rs = statement.executeQuery("SELECT papers_id FROM papers WHERE papers_title=\'" + paper.getTitle() + "\' AND papers_datesubmitted=\'" + paper.getDate() + "\';");
				int the_id = rs.getInt("papers_id");
				paper.setID(the_id);			
				my_papers.add(paper);
				setChanged();
				notifyObservers("newPaper");
				selectPaper(paper.getID());
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}
			DBConnector.closeConnection(con);	
		} else {
			System.err.println("ERROR: Please loggin as a user and select a conference before adding a paper");
		}
	}	
	
	/**
	 * Return current paper.
	 * 
	 * @return Current paper.
	 */
	public Paper getPaper() {
		return my_paper;
	}
	
	/**
	 * Returns conference to search papers based off of current conference.
	 * 
	 * @param the_conference The papers id.
	 * @return The paper associated with the provided paper id.
	 */
	public Paper getPaper(int the_paperId) {
		Paper thePaper = null;
		for (Paper p : my_papers) {
			if (p.getID() == the_paperId) {
				thePaper = p;
				break;
			}
		}
		return thePaper;
	}
	
	/**
	 * Returns a list of papers associated with current conference.
	 * 
	 * @return List of papers associated with current conference.
	 */
	public List<Paper> getPapers() {
		return my_papers;
	}
	
	/**
	 * Selects the paper which is considered current.
	 * 
	 * @param the_paper_id Id of the paper to select as current.
	 */
	public void selectPaper(int the_paper_id) {
		my_reviews.clear();
		my_recommendations.clear();
		for (Paper p : my_papers) {
			if (p.getID() == the_paper_id) {
				my_paper = p;
				my_author = p.getAuthor();
				my_programchair = my_conference.getProgramchair();
				my_subprogramchair = p.getSubprogramchair();
				my_reviewers = p.getReviewers();
				updateReviews();
				my_paper_role = p.getRole();
				updateRecommendations();
				updateParticipants();
				setChanged();				
				notifyObservers(my_paper);
				break;
			}
		}
	}
	
	public String getPaperRole() {
		return my_paper_role;
	}
	
	//
	//
	//
	// Roles
	//
	//
	//
	
	public int getUserPaperRole() {
		return my_user_paper_role;
	}
	
	public List<User> getReviewers() {
		
		
		if (my_paper != null) {
			
		}
		
		return null;
	}
	
	public User getAuthor(Paper the_paper_id) {
		return my_author;
	}
	
	public User getProgramchair(Paper the_paper_id) {
		return my_programchair;
	}
	
	public User getSubprogramchair(Paper the_paper_id) {
		return my_subprogramchair;
	}
	
	public void setSubprogramchair(User the_user) {
		if (my_logged_in_user.getID() == my_programchair.getID() && my_paper != null) {
			Connection con = DBConnector.connect();
			try {			
				Statement statement = con.createStatement();
				//ResultSet rs = statement.executeQuery("SELECT * FROM papers WHERE papers_id=\'" + my_paper.getID() + "\';");
				statement.execute("UPDATE papers Set papers_subprogramchair=\'" + the_user.getID() + "\' WHERE papers_id=\'" + my_paper.getID() + "\';");
				my_paper.setSubprogramchair(the_user);
				setChanged();
				notifyObservers(my_paper);
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}	
			DBConnector.closeConnection(con);	
			my_subprogramchair = the_user;
		}
	}
	
	public void addReviewer(User the_user) {
		if (the_user != null && !my_reviewers.contains(the_user)) {
			my_reviewers.add(the_user);
		}

	}
	
	public void addSubprogramchair(int paper_id, User user) {
		Paper thePaper = getPaper(paper_id);
	}
	
	private void updateParticipants() {
	//	my_author
	//	my_programchair
	//	my_subprogramchair
	//	my_reviewers
	}
	
	//
	//
	//
	// Review Controllers
	//	
	//
	//	
	
	/**
	 * Helper method to populate reviews based on selected my_paper.
	 */
	private void updateReviews() {
		Connection con = DBConnector.connect();
		try {
			Statement statement = con.createStatement();
			ResultSet rs1 = statement.executeQuery("SELECT * FROM reviews WHERE paper_id_fk=\'" + my_paper.getID() + "\';");
			while (rs1.next()) {
				Review r = new Review(rs1.getInt("user_paper_reviews_id"), getUser(rs1.getInt("user_id_fk")), rs1.getString("reviews_message"), rs1.getString("reviews_date"), null);
				my_reviews.add(r);
			}
		} catch (Exception ex) {
			System.err.println("Error: " + ex);
		}	
		DBConnector.closeConnection(con);
		setChanged();
		update();
	}	
	
	/**
	 * Adds a review to the selected paper.
	 * 
	 * @param the_message
	 */
	public void addReview(String the_message) {
		if (my_paper != null && my_logged_in_user != null) {	
			Connection con = DBConnector.connect();
			try {
				Review r = new Review(my_logged_in_user, the_message, null);
				Statement statement = con.createStatement();
				statement.execute("INSERT INTO reviews (user_id_fk, paper_id_fk, reviews_message, reviews_date) VALUES (\'" + my_logged_in_user.getID() + "\',\'" + my_paper.getID() + "\',\'" + r.getMessage() + "\',\'" + r.getDate() + "\');");
				Statement statement1 = con.createStatement();
				ResultSet rs = statement1.executeQuery("SELECT * FROM reviews WHERE user_id_fk=\'" + my_logged_in_user.getID() + "\' AND paper_id_fk=\'" + my_paper.getID() + "\' AND reviews_message=\'" + r.getMessage() +"\';");
				r.setID(rs.getInt("user_paper_reviews_id"));
				my_reviews.add(r);
				setChanged();
				notifyObservers(r);
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}	
			DBConnector.closeConnection(con);
		}
	}
	
	
	/**
	 * Returns the review with the associated id.
	 * 
	 * @param the_reviewID ID of review to return.
	 * @return Review with associated id, null otherwise.
	 */
	public Review getReview(int the_reviewID) {
		Review theReview = null;
		for (Review r : my_reviews) {
			if (r.getID() == the_reviewID) {
				theReview = r;
				break;
			}
		}
		return theReview;
	}
	
	/**
	 * Returns a list of reviews associated with the selected paper.
	 * @return Reviews associated with the selected paper.
	 */
	public List<Review> getReviews() {
		return my_reviews;
	}

	//
	//
	//
	// Recommendation Controllers
	//	
	//
	//	
	
	/**
	 * Helper method to populate recommendations based on selected my_paper.
	 */
	private void updateRecommendations() {
		if (my_paper.getSubprogramchair() != null && (my_paper.getSubprogramchair().getID() == my_logged_in_user.getID() || my_programchair.getID() == my_logged_in_user.getID())) {
			Connection con = DBConnector.connect();
			try {
				Statement statement = con.createStatement();
				ResultSet rs1 = statement.executeQuery("SELECT * FROM recommendations WHERE user_id_fk=\'" + my_logged_in_user.getID() + "\' AND paper_id_fk=\'" + my_paper.getID() + "\';");
				while (rs1.next()) {
					Recommendation rec = new Recommendation(rs1.getInt("recommendations_id"), getUser(rs1.getInt("user_id_fk")), rs1.getString("recommendations_message"), rs1.getString("recommendations_date"));
					my_recommendations.add(rec);
				}
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}	
			DBConnector.closeConnection(con);
		}
	}	
	
	/**
	 * Adds a recommendation to the selected paper.
	 * 
	 * @param the_message
	 */
	public void addRecommendation(String the_message) {
		if (my_paper != null && my_logged_in_user != null) {	
			Connection con = DBConnector.connect();
			try {			
				Recommendation r = new Recommendation(my_logged_in_user, the_message);
				Statement statement = con.createStatement();
				statement.execute("INSERT INTO recommendations (user_id_fk, paper_id_fk, recommendations_message, recommendations_date) VALUES (\'" + my_logged_in_user.getID() + "\',\'" + my_paper.getID() + "\',\'" + r.getMessage() + "\',\'" + r.getDate() + "\');");
				Statement statement1 = con.createStatement();
				ResultSet rs = statement1.executeQuery("SELECT * FROM recommendations WHERE user_id_fk=\'" + my_logged_in_user.getID() + "\' AND paper_id_fk=\'" + my_paper.getID() + "\' AND recommendations_message=\'" + r.getMessage() +"\';");
				r.setID(rs.getInt("recommendations_id"));
				my_recommendations.add(r);
				setChanged();
				notifyObservers(r);
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}	
			DBConnector.closeConnection(con);
		}
	}
	
	/**
	 * Returns the recommendation with the associated id.
	 * 
	 * @param the_reviewID ID of recommendation to return.
	 * @return Recommendation with associated id, null otherwise.
	 */
	public Recommendation getRecommendation(int the_reviewID) {
		Recommendation theReview = null;
		for (Recommendation r : my_recommendations) {
			if (r.getID() == the_reviewID) {
				theReview = r;
				break;
			}
		}
		return theReview;
	}
	
	/**
	 * Returns a list of recommendations associated with the selected paper.
	 * 
	 * @return Recommendations associated with the selected paper.
	 */
	public List<Recommendation> getRecommendations() {
		return my_recommendations;
	}
	
	public void addToMyConferences(int the_conference_id) {
		for (int i = 0; i < all_conferences.size(); i++) {
			if (all_conferences.get(i).getID() == the_conference_id) {
				Conference cf = all_conferences.get(i);
				Connection con = DBConnector.connect();
				try {
					Statement statement = con.createStatement();
					statement.execute("INSERT INTO user_conference_maps (userconference_user_id, userconference_conference_id) VALUES (\'" + my_logged_in_user.getID() + "\',\'" + cf.getID() + "\');");
					all_conferences.remove(i);
					my_conferences.add(cf);
					DBConnector.closeConnection(con);	
					setChanged();
					notifyObservers("AddedConference");
					break;
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}		
	}
	
	public void setStatus(String the_status) {
		if (my_logged_in_user.getID() == my_programchair.getID() && my_paper != null) {
			Connection con = DBConnector.connect();
			try {			
				Statement statement = con.createStatement();
				//ResultSet rs = statement.executeQuery("SELECT * FROM papers WHERE papers_id=\'" + my_paper.getID() + "\';");
				statement.execute("UPDATE papers Set papers_status=\'" + the_status + "\' WHERE papers_id=\'" + my_paper.getID() + "\';");
				setChanged();
				notifyObservers(my_paper);
			} catch (Exception ex) {
				System.err.println("Error: " + ex);
			}	
			DBConnector.closeConnection(con);
			my_paper.setStatus(the_status);
		}
	}
	
	public User getProgramchair() {
		return my_programchair;
	}
}
