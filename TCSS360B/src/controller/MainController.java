package controller;

import java.awt.EventQueue;

import model.User;
import view.PortalGUI;
import view.LoginGUI;

public class MainController {

	public static void main(String[] the_args) {	
		EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run() {	
            	PortalControl portal = new PortalControl();		
            	LoginControl loginController = new LoginControl(portal);    	    	
            	PortalGUI gui = new PortalGUI(loginController, portal);
            	portal.addObserver(gui);
                gui.start();    
            }
        });
	}
}
