/**
 * This class connects sqlite and java so that sql quries may take place with Statement and ResultSet classes.
 */

package controller;

import java.sql.*;

/**
 * @author Ryan Jeffries
 *
 */
public class DBConnector {
	
	/**
	 * Creates a connection between java and sqlite database. Make sure to close the connection after query, deletion, ect...
	 * 
	 * @return The connection between java and sqlite database.
	 */
	public static Connection connect() {
		Connection con = null;
		try {
			Class.forName("org.sqlite.JDBC");
		    con = DriverManager.getConnection("jdbc:sqlite::resource:db.sqlite");
		    //	    statement.setQueryTimeout(30);  // set timeout to 30 sec.   			
		} catch(Exception ex) {
			System.out.println("DBConnector.java - Connector(): Cannot connect to database " + ex);
		}
		return con;
	}
	
	/**
	 * Closes the connection between java and sqlite.
	 * 
	 * @param con The connection to close.
	 */
	public static void closeConnection(Connection con) {
	    try
	    {
	      if(con != null)
	    	  con.close();
	    }
	      catch(SQLException e)
	    {
	      // connection close failed.
	      System.err.println("DBConnector - closeConnection() " + e);
	    }
	}
	
}
