import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import model.Conference;
import controller.DBConnector;


public class LoadData {

	public static void main(String[] args) {
		
		File file = new File("data.txt");
		Scanner input = null;
		try {
			input = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection con = DBConnector.connect();
		Statement statement;

		input.nextLine();
		while(input.hasNext()) {
			//System.out.println(input.next());
		    String nextLine = input.nextLine();
			Scanner lineScan = new Scanner(nextLine);
			lineScan.useDelimiter(",");
			while (lineScan.hasNext()) {
				//System.out.println(lineScan.next());
				int userID = 99;

				if(lineScan.hasNextInt()) 
				{
					userID = lineScan.nextInt();
				}
				


				String firstName = lineScan.next();

				String lastName = lineScan.next();
	
				String email = lineScan.next();
	
				int ConferenceID = 99;
				if(lineScan.hasNextInt()) 
				{
					ConferenceID = lineScan.nextInt();
				}
				String ConferenceName = lineScan.next();
				String ConferenceDescription = lineScan.next();
				int roleID = 99;
				if(lineScan.hasNextInt()) 
				{
					roleID = lineScan.nextInt();
				}
				String roleName = lineScan.next();
				
				try {
					statement = con.createStatement();
					statement.execute("INSERT INTO users (users_id, users_first, users_last, users_email) VALUES (\'" + userID + "\',\'" + firstName + "\',\'" + lastName + "\',\'" + email + "\');");
					statement = con.createStatement();
					statement.execute("INSERT INTO userpasswords (userpasswords_user_email, userpasswords_password) VALUES (\'" + email + "\',\'" + 123 + "\');");
					statement = con.createStatement();
					statement.execute("INSERT INTO user_paper_maps (users_id_fk, roles_id_fk, papers_id_fk, conference_id_fk) VALUES (\'" + userID + "\',\'" + roleID + "\',\'" + 0 + "\',\'" + ConferenceID + "\');");
				} catch (Exception ex) {
					System.err.println("Error: " + ex);
				}	
			}

	    
		    
		    

		    
		}
		
		DBConnector.closeConnection(con);	

		input.close();
		
	}
}
