package view;

import java.awt.BorderLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JButton;

import model.User;
import controller.PortalControl;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.FlowLayout;
import java.util.List;

/**
 * 
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 */
public class ConferenceForm extends JFrame {

	private JPanel contentPane;
	private JTextArea textField;
	private JTextArea textField_1;
	private JTextArea textField_2;
	private JTextArea textField_3;
	private JTextArea textField_4;
	private PortalControl my_portal;
	/**
	 * Create the frame.
	 */
	public ConferenceForm(PortalControl the_portal) {
		my_portal = the_portal;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 245, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 10));
		
		JLabel lblConferenceName = new JLabel("Conference Name :");
		panel.add(lblConferenceName);
		
		textField = new JTextArea();
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblConferenceDescription = new JLabel("Conference Description :");
		panel.add(lblConferenceDescription);
		
		textField_1 = new JTextArea();
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblPaperDeadlineFor = new JLabel("Paper Deadline for Conference :");
		panel.add(lblPaperDeadlineFor);
		
		textField_2 = new JTextArea();
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblConferenceDate = new JLabel("Conference Date :");
		panel.add(lblConferenceDate);
		
		textField_3 = new JTextArea();
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblConferenceLocation = new JLabel("Conference Location :");
		panel.add(lblConferenceLocation);
		
		textField_4 = new JTextArea();
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblAssignProgramChair = new JLabel("Assign Program Chair :");
		panel.add(lblAssignProgramChair);

		List<User> allUsers = my_portal.getAllUsers();
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"String 1", "String 2", "String 3"}));
		panel.add(comboBox);
		
		JLabel lblAddAConference = new JLabel("Add a Conference");
		lblAddAConference.setForeground(Color.BLUE);
		lblAddAConference.setFont(new Font("Sylfaen", Font.BOLD, 42));
		lblAddAConference.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblAddAConference, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setVgap(30);
		flowLayout.setHgap(20);
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnSubmitConference = new JButton("Submit Conference");
		btnSubmitConference.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean successful = my_portal.addConference(textField.getText(), textField_1.getText(), textField_2.getText(), textField_3.getText(), textField_4.getText(), null);
				if (successful) {
					dispose();
				} else {
					JOptionPane.showMessageDialog(ConferenceForm.this, "Are you sure you wouldn't like to Register?");
				}
			}
		});
		btnSubmitConference.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(btnSubmitConference);
		
		JButton btnCancelSubmission = new JButton("Cancel Submission");
		btnCancelSubmission.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelSubmission.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(btnCancelSubmission);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setHgap(20);
		contentPane.add(panel_2, BorderLayout.WEST);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_3.getLayout();
		flowLayout_2.setHgap(20);
		contentPane.add(panel_3, BorderLayout.EAST);
	}

}
