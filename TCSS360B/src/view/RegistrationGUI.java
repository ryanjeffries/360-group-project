/**
 * 
 */
package view;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Font;

import javax.swing.SwingConstants;

import controller.LoginControl;
import controller.PortalControl;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * 
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 *
 */
public class RegistrationGUI extends JFrame {

  private JPanel contentPane;
  private JPanel panel;
  private JTextField first_name;
  private JTextField last_name;
  private JTextField email;
  private JTextField email_confirm;
  private JTextField password;
  private JTextField password_confirm;

  private PortalControl my_portal;
  
  private LoginControl my_loginM;
  
  /**
   * Create the frame.
   */
  public RegistrationGUI(PortalControl the_portal, LoginControl the_loginM) {
    my_portal = the_portal;
    my_loginM = the_loginM;
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 450, 300);
    setLocationRelativeTo(null);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new BorderLayout(0, 0));
    
    panel = new JPanel();
    contentPane.add(panel, BorderLayout.CENTER);
    panel.setLayout(new GridLayout(8, 0, 20, 0));
    
    final JLabel label = new JLabel("Enter your First Name :");
    panel.add(label);
    
    first_name = new JTextField();
    first_name.setColumns(10);
    panel.add(first_name);
    
    final JLabel label_1 = new JLabel("Enter your last name :");
    panel.add(label_1);
    
    last_name = new JTextField();
    last_name.setColumns(10);
    panel.add(last_name);
    
    final JLabel label_2 = new JLabel("Enter a valid E-mail address :");
    panel.add(label_2);
    
    email = new JTextField();
    email.setColumns(10);
    panel.add(email);
    
    final JLabel label_3 = new JLabel("Confirm E-mail :");
    panel.add(label_3);
    
    email_confirm = new JTextField();
    email_confirm.setColumns(10);
    panel.add(email_confirm);
    
    final JLabel label_4 = new JLabel("Enter a password :");
    panel.add(label_4);
    
    password = new JPasswordField();
    password.setColumns(10);
    panel.add(password);
    
    final JLabel label_5 = new JLabel("Re-enter password :");
    panel.add(label_5);
    
    password_confirm = new JPasswordField();
    password_confirm.setColumns(10);
    panel.add(password_confirm);
    
    
    
    JButton button = new JButton("Submit Registration");
    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (my_loginM.register(first_name.getText(), last_name.getText(), email.getText(), email_confirm.getText(), password.getText(), password_confirm.getText())) {
          dispose();
        } else {
          JOptionPane.showMessageDialog(null, this, "You suck!", JOptionPane.ERROR_MESSAGE);
        }
      }
    });
    panel.add(button);
    
    JButton button_1 = new JButton("Cancel Registration");
    button_1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    panel.add(button_1);
    
    JLabel lblPleaseEnterYour = new JLabel("Please Enter Your Registration Information :");
    lblPleaseEnterYour.setForeground(Color.RED);
    lblPleaseEnterYour.setHorizontalAlignment(SwingConstants.CENTER);
    lblPleaseEnterYour.setFont(new Font("Tahoma", Font.BOLD, 16));
    contentPane.add(lblPleaseEnterYour, BorderLayout.NORTH);
    
    JPanel panel_1 = new JPanel();
    contentPane.add(panel_1, BorderLayout.WEST);
    
    JPanel panel_2 = new JPanel();
    contentPane.add(panel_2, BorderLayout.EAST);
  }

}
