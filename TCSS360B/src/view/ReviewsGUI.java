/**
 * 
 */
package view;

import javax.swing.JPanel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import controller.PortalControl;
import model.Conference;
import model.Paper;
import model.Review;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 *
 */
public class ReviewsGUI extends JPanel implements Observer {
	
	private JTable table;

	private PortalControl my_portal;
	
	private JLabel my_paper_label;
	
	private int selected_row;
	
	/**
	 * Create the panel.
	 */
	public ReviewsGUI(PortalControl the_portal) {
		super();
		selected_row = -1;
		my_portal = the_portal;
		setLayout(new BorderLayout(0, 0));
		
		my_paper_label = new JLabel();
		
		JPanel labelPanel = new JPanel();
		labelPanel.add(my_paper_label, BorderLayout.CENTER);
		add(labelPanel, BorderLayout.CENTER);
		
		add(labelPanel, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable() {
		    public Component prepareRenderer(
		            TableCellRenderer renderer, int row,
		            int column) {
		        Component c = super.prepareRenderer(renderer,
		                row, column);
		        if (row == selected_row) {
		        
		        	c.setBackground(new Color(245, 245, 220));
		        } else {
		        	 c.setBackground(getBackground());
		        	 }
		        return c;
		    }
		    @Override
		    public boolean isCellEditable(int row, int column) {
		        return false;
		    }
		};
		table.setEnabled(false);
		scrollPane.setViewportView(table);

  		table.addMouseListener(new MouseAdapter() {
   			public void mouseClicked(MouseEvent e) {
      			if (e.getClickCount() == 1) {
         			selected_row = table.rowAtPoint(e.getPoint());
        			updateTable();
         		} else if (e.getClickCount() == 2) {
         			//Open review form with populated items.
         		}
   			}
		});
	}
	
	private void updateTable() {
		int columnWidth = 5;
		List<Review> rvs = my_portal.getReviews();
		Object[][] o = new Object[rvs.size()][columnWidth];
		for (int i = 0; i < rvs.size(); i++) {				
			o[i][0] = rvs.get(i).getDate();
			o[i][1] = rvs.get(i).getUser().toString();
			o[i][2] = rvs.get(i).getMessage();
		}
		table.setModel(new DefaultTableModel(
			o,
			new String[] {
				"Date", "User", "Message"
			}
		));
		my_paper_label.setText(my_portal.getPaper().getDate() + "     " + my_portal.getPaper().getTitle());		
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof Paper) {
			selected_row = -1;
			updateTable();
		} else if (arg1 instanceof Review) {
			selected_row = -1;
			updateTable();
		}
	}
}
