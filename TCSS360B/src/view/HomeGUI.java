/**
 * 
 */
package view;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 *
 */
public class HomeGUI extends JPanel {

	/**
	 * My JFrames window icon.
	 */
	final ImageIcon IMAGE = new ImageIcon(getClass().getResource("/meeting_000.jpg"));

	/**
	 * Create the panel.
	 */
	public HomeGUI() {
		super();
		
		setLayout(new BorderLayout(0, 0));

		JLabel lblWelcome = new JLabel("Welcome");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 72));
		add(lblWelcome, BorderLayout.NORTH);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setIcon(IMAGE);
		add(lblNewLabel, BorderLayout.CENTER);
		
		JLabel lblG6 = new JLabel("Group 6 Project");
		lblG6.setHorizontalAlignment(SwingConstants.CENTER);
		lblG6.setFont(new Font("Tahoma", Font.BOLD, 24));
		add(lblG6, BorderLayout.SOUTH);

	}
}
