/**
 * Group 6 Project
 */
package view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import model.Conference;
import model.Paper;
import controller.LoginControl;
import controller.PortalControl;

/**
 * The GUI to run for all program visuals.
 * 
 * @author Group 6 - 360B
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 * @version Feb 15, 2014
 */
public class PortalGUI implements Observer {
	
	/**
     * My JFrames window icon.
     */
   final ImageIcon IMAGE = new ImageIcon(getClass().getResource("/IMAGE.jpg"));
	
	/**
     * My JFrame for use by my program.
     */
    private JFrame my_frame;
    
    private int my_selected_conference;
    
    
    //GUIs
    private LoginGUI my_login_frame;
    
    private HomeGUI my_home_gui;
    
    private AllConferencesGUI all_conference_gui;
    
    private ConferencesGUI my_conference_gui;
    
    private PapersGUI my_paper_gui;
    
    private PaperGUI my_paper2_gui;    
    
    private RecommendationsGUI my_recommendation_gui;
    
    private ReviewsGUI my_review_gui;
    
    private RegistrationGUI my_registration_gui;
    
    private ConferenceForm my_conference_form;
    
    private PaperForm my_paper_form;
    
    private ReviewForm my_review_form;
    
    private RecommendationForm my_recommendation_form;
    
    
    //Panels and Screens
    private JPanel my_main_panel;
    
    private final CardLayout my_card_layout;
    
    //JMenu, JMenuItems, & JButtons
    
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	private JToolBar my_toolbar;
    
	private JMenuItem mntmAddPaper, mntmAddReview, mntmAddRecommendation, mntmAddConference;
	
	private JToggleButton tglbtnHome, tglbtnSubmissions, tglbtnRecommendation, tglbtnReviews, tglbtnPaper2;
	
	private JButton btnAddPaper, btnAddReview, btnAddRecommendation, btnAddConference;
	
	
	//Abstract Actions
    
	private AbstractAction my_home_action, all_conference_action, my_conference_action, my_paper_action, my_paper2_action, my_review_action, my_recommendation_action;
	
	
	//Controllers
	private LoginControl my_log;
	
	private PortalControl my_portal;

	/**
	 * 
	 * @param the_loginController
	 * @param the_portalController
	 */
	public PortalGUI(LoginControl the_loginController, PortalControl the_portalController) {
		//Instantiate Controllers
		   my_log = the_loginController;
		   my_portal = the_portalController;
		//Instantiagte GUIs
		   my_frame = new JFrame();
		   my_login_frame = new LoginGUI(my_portal, my_log);
		   my_card_layout = new CardLayout();
		   my_main_panel = new JPanel(my_card_layout);
		   my_toolbar = new JToolBar();
		   my_review_gui = new ReviewsGUI(my_portal);
	       my_recommendation_gui = new RecommendationsGUI(my_portal);
	       my_registration_gui = new RegistrationGUI(my_portal, my_log);
		   my_home_gui = new HomeGUI();
		   my_conference_form = new ConferenceForm(my_portal);
		   my_paper_form = new PaperForm(my_portal, my_paper_gui);
		   my_review_form = new ReviewForm(my_portal);
		   my_recommendation_form = new RecommendationForm(my_portal);
		//Instantiated GUI Observer classes
		   all_conference_gui = new AllConferencesGUI(my_portal, this);
		   my_conference_gui = new ConferencesGUI(my_portal);
		   my_paper_gui = new PapersGUI(my_portal);
		   my_paper2_gui = new PaperGUI(my_portal);
		   my_portal.addObserver(all_conference_gui);
		   my_portal.addObserver(my_conference_gui);
		   my_portal.addObserver(my_paper_gui);
		   my_portal.addObserver(my_paper2_gui);
		   my_portal.addObserver(my_review_gui);
		   my_portal.addObserver(my_recommendation_gui);
		   my_selected_conference = -1;
	}

	/**
	 * 
	 */
	public void start() {
		//sets up the GUI
		my_frame.setTitle("Group 6 Project");
        my_frame.setIconImage(IMAGE.getImage());
        my_frame.setLayout(new BorderLayout());
        my_frame.setBackground(Color.WHITE);
        my_frame.setPreferredSize(new Dimension(700, 700));
        my_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);      
        
        setUpComponents();
        
        my_frame.pack();
        my_frame.setResizable(false);
        my_frame.setLocationRelativeTo(null);
        
        authenticateLogin();
	}
	
	/**
	 * 
	 */
	public void setSelectedConference(int the_conference_int) {
		my_selected_conference = the_conference_int;
		btnAddConference.setVisible(true);
	}
	
	/**
	 * 
	 */
	private void setUpComponents() {
		//sets up the GUI components
		setUpActions();
        setUpToolBar();
        
        my_conference_form.setVisible(false);
        
        my_paper_form.setVisible(false);
        
        my_review_form.setVisible(false);
        
        my_recommendation_form.setVisible(false);
		
		my_frame.setJMenuBar(setUpJMenuBar());
		
		my_frame.add(my_toolbar, BorderLayout.NORTH);
		
		my_frame.add(addSouthPanel(), BorderLayout.SOUTH);
		
		my_card_layout.addLayoutComponent(my_home_gui, "home");
		
		my_card_layout.addLayoutComponent(all_conference_gui, "allconference");
		
		my_card_layout.addLayoutComponent(my_conference_gui, "conference");
		
		my_card_layout.addLayoutComponent(my_paper_gui, "paper");
		
		my_card_layout.addLayoutComponent(my_paper2_gui, "paper2");
		
		my_card_layout.addLayoutComponent(my_review_gui, "review");
		
		my_card_layout.addLayoutComponent(my_recommendation_gui, "recommendation");
		
		my_main_panel.add(my_home_gui);
		
		my_main_panel.add(my_conference_gui);
		
		my_main_panel.add(all_conference_gui);
		
		my_main_panel.add(my_paper_gui);
		
		my_main_panel.add(my_paper2_gui);

		my_main_panel.add(my_review_gui);
		
		my_main_panel.add(my_recommendation_gui);
		
		my_frame.add(my_main_panel, BorderLayout.CENTER);
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("serial")
	private void setUpActions() {
		my_home_action = new AbstractAction("Home") {
            public void actionPerformed(final ActionEvent the_event) {
            	switchCards("home");
            }
        };
        
        all_conference_action = new AbstractAction("All Conferences") {
            public void actionPerformed(final ActionEvent the_event) {
            	switchCards("allconference");
            }
        };
        
        my_conference_action = new AbstractAction("My Conferences") {
            public void actionPerformed(final ActionEvent the_event) {
            	switchCards("conference");
            }
        };
        
        my_paper_action = new AbstractAction("Papers") {
            public void actionPerformed(final ActionEvent the_event) {
            	switchCards("paper");
            }
        };
        
        my_paper2_action = new AbstractAction("Paper") {
            public void actionPerformed(final ActionEvent the_event) {
            	switchCards("paper2");
            }
        };
        
        my_review_action = new AbstractAction("Reviews") {
            public void actionPerformed(final ActionEvent the_event) {
            	switchCards("review");
            }
        };
        
        my_recommendation_action = new AbstractAction("Recommendations") {
            public void actionPerformed(final ActionEvent the_event) {
            	switchCards("recommendation");
            }
        };
	}
	
	/**
	 * 
	 * @return
	 */
	private JMenuBar setUpJMenuBar() {
		JMenuBar menu_bar = new JMenuBar();
		
		JMenu mnFile = new JMenu("File");
		menu_bar.add(mnFile);
		
		mntmAddConference = new JMenuItem("Create Conference");
		mntmAddConference.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				my_conference_form.setVisible(true);
				my_conference_form.setLocationRelativeTo(null);
			}
		});
		mnFile.add(mntmAddConference);
		mntmAddConference.setVisible(false);
		
		mntmAddPaper = new JMenuItem("Add Paper");
		mnFile.add(mntmAddPaper);
		mntmAddPaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Open new paper form. That form calls my_portal.addPaper(vars..);
			}
		});
		mntmAddPaper.setVisible(false);
		
		mntmAddReview = new JMenuItem("Add Review");
		mnFile.add(mntmAddReview);
		mntmAddReview.setVisible(true);
		
		mntmAddRecommendation = new JMenuItem("Add Recommendation");
		mnFile.add(mntmAddRecommendation);
		mntmAddRecommendation.setVisible(true);
		
		JMenuItem mntmLogout = new JMenuItem("Logout");
		mnFile.add(mntmLogout);
		mntmLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				my_portal.logOut();
			}
		});
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mntmAddPaper.setVisible(false);
		
		
		JMenu mnOptions = new JMenu("Options");
		menu_bar.add(mnOptions);
		
		JMenuItem mntmAboutTheProject = new JMenuItem("About the Project");
		mnOptions.add(mntmAboutTheProject);
		
		JMenuItem mntmAboutTheGroup = new JMenuItem("About the Group");
		mnOptions.add(mntmAboutTheGroup);
		
		return menu_bar;
	}
	
	/**
	 * 
	 */
	private void setUpToolBar() {
		my_toolbar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		my_toolbar.setBackground(SystemColor.activeCaption);
		
		//Home Tool Bar Button
		tglbtnHome = new JToggleButton(my_home_action);
		buttonGroup.add(tglbtnHome);
		tglbtnHome.setMnemonic('H');
		tglbtnHome.setSelected(true);
		tglbtnHome.setFont(new Font("Tahoma", Font.PLAIN, 14));
		my_toolbar.add(tglbtnHome);
		
		//Conference Tool Bar Button
		JToggleButton tglbtnAllConferences = new JToggleButton(all_conference_action);
		buttonGroup.add(tglbtnAllConferences);
		tglbtnAllConferences.setMnemonic('C');
		tglbtnAllConferences.setFont(new Font("Tahoma", Font.PLAIN, 14));
		my_toolbar.add(tglbtnAllConferences);
		tglbtnAllConferences.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mntmAddPaper.setVisible(false);
				btnAddPaper.setVisible(false);
				tglbtnSubmissions.setVisible(false);
				btnAddReview.setVisible(false);
				btnAddRecommendation.setVisible(false);
				tglbtnReviews.setVisible(false);
				tglbtnPaper2.setVisible(false);
				tglbtnRecommendation.setVisible(false);
				btnAddConference.setVisible(true);
			}
		});	
		
		//Conference Tool Bar Button
		JToggleButton tglbtnConferences = new JToggleButton(my_conference_action);
		buttonGroup.add(tglbtnConferences);
		tglbtnConferences.setMnemonic('C');
		tglbtnConferences.setFont(new Font("Tahoma", Font.PLAIN, 14));
		my_toolbar.add(tglbtnConferences);
		tglbtnConferences.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mntmAddPaper.setVisible(false);
				btnAddPaper.setVisible(false);
				tglbtnSubmissions.setVisible(false);
				btnAddReview.setVisible(false);
				btnAddRecommendation.setVisible(false);
				tglbtnReviews.setVisible(false);
				tglbtnPaper2.setVisible(false);
				tglbtnRecommendation.setVisible(false);
				btnAddConference.setVisible(false);
			}
		});			

		//Papers Tool Bar Button
		tglbtnSubmissions = new JToggleButton(my_paper_action);
		buttonGroup.add(tglbtnSubmissions);
		tglbtnSubmissions.setMnemonic('P');
		tglbtnSubmissions.setFont(new Font("Tahoma", Font.PLAIN, 14));
		my_toolbar.add(tglbtnSubmissions);
		tglbtnSubmissions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mntmAddPaper.setVisible(true);
				btnAddPaper.setVisible(true);
				tglbtnPaper2.setVisible(false);
				btnAddReview.setVisible(false);
				btnAddRecommendation.setVisible(false);
				tglbtnReviews.setVisible(false);
				tglbtnRecommendation.setVisible(false);
				mntmAddReview.setVisible(false);
				mntmAddRecommendation.setVisible(false);
			}
		});
		tglbtnSubmissions.setVisible(false);
		
		//Paper Tool Bar Button
		tglbtnPaper2 = new JToggleButton(my_paper2_action);
		buttonGroup.add(tglbtnPaper2);
		tglbtnPaper2.setMnemonic('W');
		tglbtnPaper2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		my_toolbar.add(tglbtnPaper2);
		tglbtnPaper2.setVisible(false);
		
		tglbtnReviews = new JToggleButton(my_review_action);
		buttonGroup.add(tglbtnReviews);
		tglbtnReviews.setMnemonic('R');
		tglbtnReviews.setFont(new Font("Tahoma", Font.PLAIN, 14));
		my_toolbar.add(tglbtnReviews);
		tglbtnReviews.setVisible(false);
		
		tglbtnRecommendation = new JToggleButton(my_recommendation_action);
		buttonGroup.add(tglbtnRecommendation);
		tglbtnRecommendation.setMnemonic('e');
		tglbtnRecommendation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		my_toolbar.add(tglbtnRecommendation);
		tglbtnRecommendation.setVisible(false);
	}
	
	/**
	 * 
	 */
	private void authenticateLogin() {
		if (!my_portal.isLoggedIn()) {
			my_login_frame.clearTextFields();
			my_login_frame.setVisible(true);
			my_frame.setVisible(false);
		} else {
			my_login_frame.clearTextFields();
			my_login_frame.setVisible(false);
			my_frame.setVisible(true);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private JPanel addSouthPanel() {
		JPanel add_panel = new JPanel();
		add_panel.setBackground(SystemColor.activeCaption);
		
	    btnAddConference = new JButton("Add Conference");
		add_panel.add(btnAddConference);
		btnAddConference.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				my_portal.addToMyConferences(my_selected_conference);
			}
		});
		btnAddConference.setVisible(false);
	    
		btnAddPaper = new JButton("Add Paper");
		add_panel.add(btnAddPaper);
		btnAddPaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				my_paper_form.setVisible(true);
				my_paper_form.setLocationRelativeTo(null);
			}
		});
		btnAddPaper.setVisible(false);
		
		btnAddReview = new JButton("Add Review");
		add_panel.add(btnAddReview);
		btnAddReview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				my_review_form.setVisible(true);
				my_review_form.setLocationRelativeTo(null);
			}
		});
		btnAddReview.setVisible(false);
		
		btnAddRecommendation = new JButton("Add Recommssendation");
		add_panel.add(btnAddRecommendation);
		btnAddRecommendation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				my_recommendation_form.setVisible(true);
				my_recommendation_form.setLocationRelativeTo(null);
			}
		});
		btnAddRecommendation.setVisible(false);
		return add_panel;
	}
	
	private void switchCards(String current_panel) {
		my_card_layout.show(my_main_panel, current_panel);
	}

	
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof String && (((String) arg1).equals("LgI") || ((String) arg1).equals("LgO"))) {
			mntmAddConference.setVisible(false);
			if (my_portal.isAdmin()) {
				mntmAddConference.setVisible(true);
			}
			my_card_layout.show(my_main_panel, "home");
			tglbtnHome.setSelected(true);
			mntmAddPaper.setVisible(false);
			btnAddPaper.setVisible(false);
			tglbtnSubmissions.setVisible(false);
			btnAddReview.setVisible(false);
			btnAddRecommendation.setVisible(false);
			tglbtnReviews.setVisible(false);
			tglbtnPaper2.setVisible(false);
			tglbtnRecommendation.setVisible(false);
			mntmAddReview.setVisible(false);
			mntmAddRecommendation.setVisible(false);
			authenticateLogin();
		} else if (arg1 instanceof Conference) {
			my_card_layout.show(my_main_panel, "paper");
			tglbtnSubmissions.setSelected(true);
			mntmAddPaper.setVisible(true);
			btnAddPaper.setVisible(true);
			tglbtnSubmissions.setVisible(true);
			btnAddConference.setVisible(false);
		} else if (arg1 instanceof Paper) {
			mntmAddPaper.setVisible(false);
			btnAddPaper.setVisible(false);

			tglbtnPaper2.setVisible(true);
			tglbtnPaper2.setSelected(true);
			if (my_portal.getPaperRole().equals("Program Chair") || my_portal.getPaperRole().equals("Subprogram Chair")) {
				tglbtnRecommendation.setVisible(true);		
				btnAddRecommendation.setVisible(true);
				mntmAddRecommendation.setVisible(true);
				btnAddReview.setVisible(true);
				mntmAddReview.setVisible(true);
			} else if (my_portal.getPaperRole().equals("Reviewer")) {
				btnAddReview.setVisible(true);
				mntmAddReview.setVisible(true);
			}
			tglbtnReviews.setVisible(true);
			btnAddConference.setVisible(false);
			tglbtnPaper2.setName(((Paper) arg1).getID() + "");
			my_card_layout.show(my_main_panel, "paper2");
		}
	}

}
