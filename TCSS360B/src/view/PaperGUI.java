/**
 * 
 */
package view;

import javax.swing.JPanel;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import controller.PortalControl;
import model.Paper;
import model.User;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 *
 */
public class PaperGUI extends JPanel implements Observer {

	private PortalControl my_portal;
	
	private JLabel my_paper_label;
	
	private JLabel my_paper_decision;
	
	private JLabel my_paper_title;
	
	private JLabel my_paper_full_text;
 	
	JComboBox<User> comboBox;
	
	JComboBox<User> comboBox_1;
	
	/**
	 * Create the panel.
	 */
	public PaperGUI(PortalControl the_portal) {
		super();
		my_portal = the_portal;
		
		setLayout(new GridLayout(7, 0, 0, 60));
		
		my_paper_decision = new JLabel();
		my_paper_label = new JLabel();
		my_paper_title = new JLabel();
		my_paper_full_text = new JLabel();
		
		JPanel labelPanel0 = new JPanel();
		JPanel labelPanel3 = new JPanel();
		JPanel labelPanel2 = new JPanel();
		JPanel labelPanel = new JPanel();
		
		labelPanel0.add(my_paper_decision);
		labelPanel.add(my_paper_title);
		labelPanel2.add(my_paper_label);
		labelPanel3.add(my_paper_full_text);
		
		add(labelPanel);
		add(labelPanel0);
		add(labelPanel2);
		add(labelPanel3);

		
  		JLabel lblAddADecision = new JLabel("Acceptance Decision");
		add(lblAddADecision);
		
		
		final JComboBox decision = new JComboBox();
		decision.setModel(new DefaultComboBoxModel(new String[] {"yes", "no", "undecided"}));
		decision.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		        my_portal.setStatus(decision.getSelectedItem().toString());
		    }
		});
		add(decision);
		
  		JLabel lblAddAReviewer = new JLabel("Add a Reviewer");
		add(lblAddAReviewer);
		

		
		comboBox = new JComboBox<User>();
		comboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	my_portal.setSubprogramchair((User)comboBox.getSelectedItem());
		    }
		});
		add(comboBox);
		JLabel lblAddASub = new JLabel("Add a Sub Program Chair");

		add(lblAddASub);
		
		comboBox_1 = new JComboBox<User>();
		add(comboBox_1);
		

	}
	

	
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof Paper) {
			my_paper_label.setText(my_portal.getPaper().getAbstract() + "\n");
			my_paper_title.setText(my_portal.getPaper().getTitle() + "\n");
			List<User> allUsers = my_portal.getAllUsers();
			
			User[] au = new User[allUsers.size()];
			
	        for (int i = 0; i < au.length; i++) {
	        	au[i] = allUsers.get(i);
	        }
  
	        DefaultComboBoxModel cboNewModel = new DefaultComboBoxModel(au);  
	        comboBox.setModel(cboNewModel);  
	        comboBox.showPopup();
	        
	        comboBox_1.setModel(cboNewModel);  
	        comboBox_1.showPopup();
	        
		}
		


	}
}