/**
 * 
 */
package view;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import controller.PortalControl;
import model.Conference;
import model.Paper;

/**
 * 
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 *
 */
public class PapersGUI extends JPanel implements Observer {
	private JTable table;

	private PortalControl my_portal;
	
	private JLabel my_conference_label;
	
	private int selected_row;
	
	/**
	 * Create the panel.
	 */
	public PapersGUI(PortalControl the_portal) {
		super();
		selected_row = -1;
		my_portal = the_portal;
		setLayout(new BorderLayout(0, 0));
		
		my_conference_label = new JLabel();
		
		JPanel labelPanel = new JPanel();
		labelPanel.add(my_conference_label, BorderLayout.CENTER);
		add(labelPanel, BorderLayout.NORTH);

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		table = new JTable() {
		    public Component prepareRenderer(
		            TableCellRenderer renderer, int row,
		            int column) {
		        Component c = super.prepareRenderer(renderer,
		                row, column);
		        if (row == selected_row) {
		        
		        	c.setBackground(new Color(245, 245, 220));
		        } else {
		        	 c.setBackground(getBackground());
		        	 }
		        return c;
		    }
		    @Override
		    public boolean isCellEditable(int row, int column) {
		        return false;
		    }
		};
		table.setEnabled(false);
		scrollPane.setViewportView(table);
  		table.addMouseListener(new MouseAdapter() {
   			public void mouseClicked(MouseEvent e) {
      			if (e.getClickCount() == 1) {
         			selected_row = table.rowAtPoint(e.getPoint());
        			updateTable();
         		} else if (e.getClickCount() == 2) {
         			int selRow = table.rowAtPoint(e.getPoint());
         			int index = 5;
         			if (my_portal.getProgramchair().getID() == my_portal.getUser().getID()) {
         				index++;
         			}
         			my_portal.selectPaper((int) table.getValueAt(selRow, index));
         		}
   			}
		});
	}
	
	public void updateTable() {
		if (my_portal.getProgramchair().getID() == my_portal.getUser().getID()) {
			int columnWidth = 7;
			List<Paper> paps = my_portal.getPapers();
			Object[][] o = new Object[paps.size()][columnWidth];
			
			for (int i = 0; i < paps.size(); i++) {
				o[i][0] = paps.get(i).getDate();
				o[i][1] = paps.get(i).getTitle();
				o[i][2] = paps.get(i).getAbstract();
				o[i][3] = paps.get(i).getStatus();
				o[i][4] = paps.get(i).getRole();
				o[i][5] = paps.get(i).getSubprogramchair().toString();
				o[i][6] = paps.get(i).getID();
			}
			table.setModel(new DefaultTableModel(
				o,
				new String[] {
					"Date Submitted", "Title", "Abstract", "Status", "Role", "Subprogram", "ID"
				}
			));
		} else {
			int columnWidth = 6;
			List<Paper> paps = my_portal.getPapers();
			Object[][] o = new Object[paps.size()][columnWidth];
			
			for (int i = 0; i < paps.size(); i++) {
				o[i][0] = paps.get(i).getDate();
				o[i][1] = paps.get(i).getTitle();
				o[i][2] = paps.get(i).getAbstract();
				o[i][3] = paps.get(i).getStatus();
				o[i][4] = paps.get(i).getRole();				
				o[i][5] = paps.get(i).getID();
			}
			table.setModel(new DefaultTableModel(
				o,
				new String[] {
					"Date Submitted", "Title", "Abstract", "Status", "Role", "ID"
				}
			));
		}
		

		my_conference_label.setText(my_portal.getConference().getDate() + "     " + my_portal.getConference().getName() + "     " + my_portal.getConference().getLocation());
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {		
		if (arg1 instanceof Conference || arg1 instanceof Paper) {
			selected_row = -1;
			updateTable();
		} else if (arg1 instanceof String && ((String) arg1).equals("newPaper")) {
			updateTable();
		}
	}

}
