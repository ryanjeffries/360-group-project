package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.Color;

import javax.swing.JTextField;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import controller.PortalControl;

public class ReviewForm extends JFrame {

	private JPanel contentPane;
	private JTextArea textField;
	private PortalControl pc;
	/**
	 * Create the frame.
	 */
	public ReviewForm(PortalControl portal) {
	  pc = portal;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 650);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(245, 245, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblAddAReview = new JLabel("Add a Review to a Paper");
		lblAddAReview.setForeground(Color.BLUE);
		lblAddAReview.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddAReview.setFont(new Font("Sylfaen", Font.BOLD, 42));
		contentPane.add(lblAddAReview, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		textField = new JTextArea();
		panel.add(textField);
		textField.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setHgap(20);
		contentPane.add(panel_1, BorderLayout.WEST);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setHgap(20);
		contentPane.add(panel_2, BorderLayout.EAST);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_3.getLayout();
		flowLayout_2.setVgap(30);
		flowLayout_2.setHgap(20);
		contentPane.add(panel_3, BorderLayout.SOUTH);
		
		JButton btnSubmitReview = new JButton("Submit Review");
		btnSubmitReview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pc.addReview(textField.getText());
				dispose();
		        textField.setText("");
			}
		});
		btnSubmitReview.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_3.add(btnSubmitReview);
		
		JButton btnCancelSubmision = new JButton("Cancel Submission");
		btnCancelSubmision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
		        textField.setText("");
			}
		});
		btnCancelSubmision.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_3.add(btnCancelSubmision);
	}

}

