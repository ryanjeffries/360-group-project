package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.GridLayout;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

import controller.PortalControl;

public class RecommendationForm extends JFrame {

	private JPanel contentPane;
	private PortalControl pc;
	private JTextArea textArea;
	/**
	 * Create the frame.
	 */
	public RecommendationForm(PortalControl portal) {
	  pc = portal;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 650);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(245, 245, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblAddARecommendation = new JLabel("Add a Recommendation to a Paper");
		lblAddARecommendation.setForeground(Color.BLUE);
		lblAddARecommendation.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddARecommendation.setFont(new Font("Sylfaen", Font.BOLD, 32));
		contentPane.add(lblAddARecommendation, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		textArea = new JTextArea();
		panel.add(textArea);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setVgap(30);
		flowLayout.setHgap(20);
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnSubmitRecommendation = new JButton("Submit Recommendation");
		btnSubmitRecommendation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSubmitRecommendation.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        pc.addRecommendation(textArea.getText());
        dispose();
        textArea.setText("");
      }
		  
		});
		panel_1.add(btnSubmitRecommendation);
		
		JButton btnCancelSubmission = new JButton("Cancel Submission");
		btnCancelSubmission.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
		        textArea.setText("");
			}
		});
		btnCancelSubmission.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(btnCancelSubmission);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setHgap(20);
		contentPane.add(panel_2, BorderLayout.WEST);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_3.getLayout();
		flowLayout_2.setHgap(20);
		contentPane.add(panel_3, BorderLayout.EAST);
	}

}

