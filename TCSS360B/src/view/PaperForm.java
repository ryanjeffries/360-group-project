package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.GridLayout;

import javax.swing.JButton;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JTextField;

import controller.PortalControl;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PaperForm extends JFrame {

	private JPanel contentPane;
	private JTextArea paperTitle;
	private JTextArea paperText;
	private JTextArea submitDate;
	private JTextArea paperAbstract;
	private PortalControl pc;
	private PapersGUI my_papers;
	/**
	 * Create the frame.
	 */
	public PaperForm(PortalControl portal, PapersGUI the_papers) {
	  pc = portal;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 650);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 245, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		my_papers = the_papers;
		
		JLabel lblAddAPaper = new JLabel("Add a Paper");
		lblAddAPaper.setForeground(Color.BLUE);
		lblAddAPaper.setBackground(new Color(245, 245, 220));
		lblAddAPaper.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddAPaper.setFont(new Font("Sylfaen", Font.BOLD, 42));
		contentPane.add(lblAddAPaper, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(4, 0, 0, 10));
		
		JLabel lblPaperTitle = new JLabel("Paper Title :");
		panel.add(lblPaperTitle);
		
		paperTitle = new JTextArea();
		panel.add(paperTitle);
		paperTitle.setColumns(10);
		
		JLabel lblFullTextOf = new JLabel("Full Text of Paper :");
		panel.add(lblFullTextOf);
		
		paperText = new JTextArea();
		panel.add(paperText);
		paperText.setColumns(10);
				
		JLabel lblPaperAbstract = new JLabel("Paper Abstract :");
		panel.add(lblPaperAbstract);
		
		paperAbstract = new JTextArea();
		panel.add(paperAbstract);
		paperAbstract.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_1.getLayout();
		flowLayout_2.setVgap(30);
		flowLayout_2.setHgap(20);
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JButton btnSubmitPaper = new JButton("Submit Paper");
		btnSubmitPaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pc.addPaper(paperTitle.getText(), paperAbstract.getText(), null);
				//my_papers.updateTable();
				dispose();
			}
		});
		btnSubmitPaper.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(btnSubmitPaper);
		
		JButton btnCancelSubmission = new JButton("Cancel Submission");
		btnCancelSubmission.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelSubmission.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(btnCancelSubmission);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
		flowLayout.setHgap(20);
		contentPane.add(panel_2, BorderLayout.WEST);
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_3.getLayout();
		flowLayout_1.setHgap(20);
		contentPane.add(panel_3, BorderLayout.EAST);

	}

}
