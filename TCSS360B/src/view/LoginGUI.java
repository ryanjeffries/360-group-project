/*
 * Group 6 Project 
 */
package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import model.User;
import controller.LoginControl;
import controller.PortalControl;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * The Login panel for use in the GUI.
 * 
 * @author Group 6
 * @version Feb 22, 2014
 */
@SuppressWarnings("serial")
public class LoginGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	public boolean logged_in=false;
	
	private int loggedInAttempts;
	
    private LoginControl my_loginM;
    
    private RegistrationGUI my_registration_gui;

	/**
	 * Create the frame.
	 */
	public LoginGUI(PortalControl the_portal, LoginControl the_log) {
		my_loginM = the_log;
		my_registration_gui = new RegistrationGUI(the_portal, the_log);
		my_registration_gui.setVisible(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JLabel lblLogin = new JLabel("Login");
		lblLogin.setForeground(Color.RED);
		lblLogin.setFont(new Font("Tekton Pro Cond", Font.BOLD | Font.ITALIC, 40));
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblLogin, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(2, 2, 10, 20));

		JLabel lblUsername = new JLabel("Username(email) :");
		lblUsername.setForeground(Color.BLUE);
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsername.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
		panel.add(lblUsername);

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(textField);
		textField.setColumns(10);

		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setForeground(Color.BLUE);
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
		panel.add(lblPassword);

		passwordField = new JPasswordField();
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(passwordField);

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setVgap(20);
		flowLayout.setHgap(35);
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		loggedInAttempts = 0;

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			  logged_in=true;
			  // set changed here?
				String username_text = textField.getText();
				char[] password_chars = passwordField.getPassword();

				StringBuilder sb = new StringBuilder();

				for (char c : password_chars) {
					sb.append(c);
				}

				String password_string = sb.toString();

				login(username_text, password_string);
				
			}
		});
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_1.add(btnLogin);

//		JButton btnSignUp = new JButton("Sign Up");
//		btnSignUp.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				my_registration_gui.setVisible(true);
//			}
//		});
//		btnSignUp.setFont(new Font("Tahoma", Font.PLAIN, 18));
//		panel_1.add(btnSignUp);
		
		pack();
        setResizable(false);
        setLocationRelativeTo(null);
	}

	public void login(String the_email, String the_password) {
		if (my_loginM.login(the_email, the_password)) {
			my_loginM.getUser(the_email);
			loggedInAttempts = 0;
		} else {
			if (loggedInAttempts > 1) {
				JOptionPane.showMessageDialog(this, "Please contact support!");
				passwordField.setText("");				
			} else {
				JOptionPane.showMessageDialog(this, "Incorrect email or password!");
				loggedInAttempts++;
			}

		}
	}
	
	public void clearTextFields() {
		textField.setText("");
		passwordField.setText("");
	}

	public void register(String the_first, String the_last, String the_email, String the_email_confir, String the_password, String the_password_confir) {
		if (my_loginM.register(the_first, the_last, the_email, the_email_confir, the_password, the_password_confir)) {
			//Tell user registration was successful, take them back to sign in, have them sign in.
		} else {
			JOptionPane.showMessageDialog(this, "Registration Fail: Ensure emails and passwords match or user already exists!");
		}
	}

}
