/*
 * Group 6 Project 
 */
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.JScrollPane;

import model.Conference;
import controller.PortalControl;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 *
 */
public class AllConferencesGUI extends JPanel implements Observer {
	
	private int selected_row;
	
	private JTable table;
	
	private PortalControl my_portal;
	
	private PortalGUI my_portal_gui;
	
	public AllConferencesGUI(PortalControl the_portal, PortalGUI the_portal_gui) {
		super();
		selected_row = -1;
		my_portal = the_portal;
		my_portal_gui = the_portal_gui;
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		
		
		table = new JTable() {
		    public Component prepareRenderer(
		            TableCellRenderer renderer, int row,
		            int column) {
		        Component c = super.prepareRenderer(renderer,
		                row, column);
		        if (row == selected_row) {
		        
		        	c.setBackground(new Color(245, 245, 220));
		        } else {
		        	 c.setBackground(getBackground());
		        	 }
		        return c;
		    }
		    @Override
		    public boolean isCellEditable(int row, int column) {
		        return false;
		    }
		};
		
		table.setEnabled(false);
		//table.setBackground(new Color(245, 245, 220));

		scrollPane.setViewportView(table);
	
		
  		table.addMouseListener(new MouseAdapter() {
   			public void mouseClicked(MouseEvent e) {
      			if (e.getClickCount() == 1) {
         			selected_row = table.rowAtPoint(e.getPoint());
         			my_portal_gui.setSelectedConference((int) table.getValueAt(selected_row, 4));
        			updateTable();
         		}
   			}
		});
	}
	
	private void updateTable() {

		int columnWidth = 5;
		List<Conference> acfs = my_portal.getAllConferences();
		List<Conference> mcfs = my_portal.getConferences();
		List<Conference> acfs2 = new ArrayList<Conference>();
		for (int i = 0; i < acfs.size(); i++) {
			if (!mcfs.contains(acfs.get(i))) {
				acfs2.add(acfs.get(i));
			}
		}
		Object[][] o = new Object[acfs2.size()][columnWidth];
		for (int i = 0; i < acfs2.size(); i++) {				
			o[i][0] = acfs2.get(i).getDate();
			o[i][1] = acfs2.get(i).getName();
			o[i][2] = acfs2.get(i).getLocation();
			o[i][3] = acfs2.get(i).getPaperdeadline();
			o[i][4] = acfs2.get(i).getID();
		}

		table.setModel(new DefaultTableModel(
			o,
			new String[] {
				"Date", "Conference Name", "Location", "Papers Due", "ID"
			}
		));
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 instanceof String && (((String) arg1).equals("LgI") || ((String) arg1).equals("NewCon"))) {
			selected_row = -1;
			updateTable();
		} else if (arg1 instanceof String && ((String) arg1).equals("AddedConference")) {
			selected_row = -1;
			updateTable();
		}
	}

	 
}
