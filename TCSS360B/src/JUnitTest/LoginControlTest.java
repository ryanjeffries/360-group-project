/**
 * This test class is mainly to test LoginControl.java
 * 
 * @author Titus Cheng
 * 
 */

package JUnitTest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import controller.LoginControl;
import controller.PortalControl;
import controller.DBConnector;

import java.sql.*;

import model.User;

public class LoginControlTest {
	
	private LoginControl loginControl;
	private PortalControl portalControl;
	private final String test_fname = "John";
	private final String test_lname = "Doe";
	private final String test_email = "Doe@example.com";
	private final String test_password = "jd123";
	private Connection dbConnect;
	private boolean isRegistered = false;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		dbConnect = DBConnector.connect();
		portalControl = new PortalControl();
		loginControl = new LoginControl(portalControl);
		
	}

	@After
	public void tearDown() throws Exception {
		if(isRegistered) {
			Statement statement = dbConnect.createStatement();
			statement.execute("delete from userpasswords where userpasswords_password=\"" + test_password + "\"");
			statement.execute("delete from users where users_email=\"" + test_email + "\"");
		}
	}
	
	/**
	 * Using test data to test if new user registration is successful.
	 */
	@Test
	public void testNewUserRegistration()
	{
		isRegistered = loginControl.register(test_fname, test_lname, test_email, test_email, test_password, test_password);
		assertTrue("LoginControlTest: registration failed", isRegistered);
		assertTrue("LoginControlTest: newly registered user login failed", loginControl.login(test_email, test_password));
	}
	
	@Test
	public void testGetUser()
	{	
		//Test for 
		assertFalse("LoginControlTest: testGetUser() empty field should not get User instance", loginControl.getUser("") instanceof User);
		assertTrue("LoginControlTest: testGetuser() valid login shoudl return User object", loginControl.getUser("Titus@example.com") instanceof User);
		
	}
	@Test
	public void testCheckPassword()
	{
		assertFalse("LoginControlTest: techCheckPassword() should return false when field is empty", loginControl.checkPassword("", ""));
		assertTrue("LoginControltest: testCheckPassword() valid long should return true", loginControl.checkPassword("Titus@example.com", "Titus2go"));
	}
	
	

}

