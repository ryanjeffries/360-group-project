package JUnitTest;

import java.sql.Connection;
import controller.DBConnector;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DBConnectorTest {

	@Test
	public void testDBConnection() {
		assertNotNull("DBConnectorTest - testDBConnection() connection is null", DBConnector.connect());	
		assertTrue("DBConnectorTest - testDBConnection() connect should be type DBConnector", DBConnector.connect() instanceof Connection);
	}
	

}
