package JUnitTest;

import model.Conference;
import model.User;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConferenceTest {
	
	private final int test_id = 22;
	private final String test_confname = "Hackathon in Seattle";
	private final String test_confdesc = "Once a year event inviting all hacking to join and exciting 24 hours project hacking";
	private final String test_date = "3-14-14";
	private final String test_deadline = "3-19-14";
	private final String test_location = "Seattle, WA";
	private final User test_user = new User(83, "Titus", "Cheng", "Titus@example.com");
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetName() {
		Conference myConference = new Conference(test_id, test_confname, test_confdesc, test_date, test_deadline, test_location, test_user);
		assertTrue("ConferenceTest: testGetName should return a valid value", test_confname.equals(myConference.getName()));
		assertTrue("ConferenceTest: testGetName should return a valid value", myConference.getName() instanceof String);
	}
	
	@Test
	public void testGetId() {
		Conference myConference = new Conference(test_id, test_confname, test_confdesc, test_date, test_deadline, test_location, test_user);
		assertTrue("ConferenceTest: testID", myConference.getID() == test_id);
	}
	
	@Test
	public void testDescription() {
		Conference myConference = new Conference(test_id, test_confname, test_confdesc, test_date, test_deadline, test_location, test_user);
		assertTrue("ConferenceTest: testDescription", test_confdesc.equals(myConference.getDescription()));

	}
	
	@Test
	public void testDeadline() {
		Conference myConference = new Conference(test_id, test_confname, test_confdesc, test_date, test_deadline, test_location, test_user);
		assertTrue("ConferenceTest: testDeadline", test_deadline.equals(myConference.getPaperdeadline()));
	}
	
	@Test
	public void testGetLocation() {
		Conference myConference = new Conference(test_id, test_confname, test_confdesc, test_date, test_deadline, test_location, test_user);
		assertTrue("ConferenceTest: testGetLocation", test_location.equals(myConference.getLocation()));
	}
	
	@Test
	public void testSetLocation() {
		Conference myConference = new Conference(test_id, test_confname, test_confdesc, test_date, test_deadline, test_location, test_user);
		final String new_location = "Bellevue, WA";
		myConference.setLocation(new_location);
		assertTrue("ConferenceTest: testSetLocation", new_location.equals(myConference.getLocation()));
	}
	
	@Test
	public void testGetProgramChair() {
		Conference myConference = new Conference(test_id, test_confname, test_confdesc, test_date, test_deadline, test_location, test_user);
		assertTrue("ConferenceTest: testID", test_user.getID() == myConference.getProgramchair().getID());
		assertTrue("ConferenceTest: testID", test_user.toString().equals(myConference.getProgramchair().toString()));
	}

}
