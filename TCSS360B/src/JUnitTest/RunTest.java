/**
 * The entry point to run all tests
 * 
 * @author Titus Cheng
 */


package JUnitTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	ConferenceTest.class,
	DBConnectorTest.class,
	LoginControlTest.class,
	PortalControlTest.class
})

public class RunTest {   
}  
