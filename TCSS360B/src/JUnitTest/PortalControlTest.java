/*
 * This class primary test PortalControl.java
 * 
 * @author Titus Cheng
 */

package JUnitTest;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

import controller.DBConnector;
import controller.PortalControl;
import model.User;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PortalControlTest {
	
	private PortalControl portalControl;
	private Connection dbConnect;
	private final int test_id = 83;
	private final String test_fname = "Titus";
	private final String test_lname = "Cheng";
	private final String test_email = "Titus@example.com";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		portalControl = new PortalControl();
		dbConnect = DBConnector.connect();
	}

	@After
	public void tearDown() throws Exception {
		Statement statement = dbConnect.createStatement();
		statement.execute("delete from conferences where conferences_programchair_id=" + test_id);
	}

	@Test
	public void testIsLoggedIn()
	{
		final User test_user = new User(test_id, test_fname, test_lname, test_email);
		portalControl.logIn(test_user, false);
		
		//test if user is logged in
		assertTrue("PortalControlTest - testIsLoggedIn() should return true if logged in", portalControl.isLoggedIn());
	}
	
	@Test
	public void testGetUser()
	{
		final User test_user = new User(test_id, test_fname, test_lname, test_email);
		portalControl.logIn(test_user, false);
		final User result_user = portalControl.getUser();
		assertTrue("PortalControlTest - testGetUser() id should be the same for logged in user", test_user.getID() == result_user.getID());
		assertTrue("PortalControlTest - testGetUser() should have the same string", test_user.toString().equals(result_user.toString()));
		
	}
	
	@Test
	public void testAddConference()
	{
		final String test_conference_name = "Hackathon";
		final String test_conference_description = "A hacking event where programmers build a project for 24 hours";
		final String test_deadline = "3-14-14";
		final String test_date = "3-10-14";
		final String test_location = "Seattle, WA";
		final User test_user = new User(test_id, test_fname, test_lname, test_email);
		assertTrue("PortalControlTest: testAddConference() failed to add conference", portalControl.addConference(test_conference_name, test_conference_description, test_deadline, test_date, test_location, test_user));
	}

}
