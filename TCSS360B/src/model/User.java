/*
 * 
 */
package model;

import java.util.List;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 * @author Titus Cheng
 *
 */
public class User {

  private int my_user_id;
  private String my_first_name;
  private String my_last_name;
  private String my_email;
  
  /**
   * 
   * @param user_id
   * @param firstName
   * @param lastName
   * @param email
   */
  public User(int user_id, String firstName, String lastName, String email) {
    my_user_id = user_id;
    my_first_name = firstName;
    my_last_name = lastName;
    my_email = email;
  }

  /**
   * 
   * @return
   */
  public int getID() {
    return my_user_id;
  }
  
  /**
   * 
   */
  @Override
  public String toString() {
	  return my_first_name + " " + my_last_name;
  }
}
