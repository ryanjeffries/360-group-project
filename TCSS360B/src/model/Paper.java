/*
 * 
 */

package model;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 * @author Titus Cheng
 *
 */
public class Paper {
  
  private int my_id;
	
	private String my_title;
	
	private String my_abstract;
	
	private String my_file;
	
	private String my_role;
	
	private String my_submitted_date;
	
	private User my_author, my_subprogramchair;
	
	private List<User> my_reviewers;
	
	private String my_status;
	
	/**
	 * 
	 * @param the_title
	 * @param the_abstract
	 * @param the_file
	 */
	public Paper(String the_title, String the_abstract, String the_file, String the_status, User the_author, User the_subprogramchair, List<User> the_reviewers, String the_role) {
		my_title = the_title;
		my_abstract = the_abstract;
		my_file = the_file;
		my_submitted_date = new Date().toString();
		my_author = the_author;
		my_subprogramchair = the_subprogramchair;
		my_reviewers = the_reviewers;
		my_status = the_status;
		my_role = the_role;
	}
	
	/**
	 * 
	 * @param the_id
	 * @param the_title
	 * @param the_abstract
	 * @param the_file
	 */
	public Paper(int the_id, String the_title, String the_abstract, String the_file) {
		my_id = the_id;
		my_title = the_title;
		my_abstract = the_abstract;
		my_file = the_file;
		my_submitted_date = new Date().toString();
		my_status = "Submitted";
	}
	
	public void setStatus(String the_status) {
		my_status = the_status;
	}
	
	public String getStatus() {
		return my_status;
	}
	
	/**
	 * 
	 * @param the_id
	 */
	public void setID(int the_id) {
		my_id = the_id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDate() {
		return my_submitted_date;
	}
	
	/**
	 * 
	 * @param the_file
	 * @return
	 */
	public String resubmitFile(String the_file) {
		my_file = the_file;
		my_submitted_date = new Date().toString();
		return my_submitted_date;
	}
	
	/**
	 * 
	 * @param the_title
	 */
	public void setTitle(String the_title) {
		my_title = the_title;
	}
	
	/**
	 * 
	 * @param the_abstract
	 */
	public void setAbstract(String the_abstract) {
		my_abstract = the_abstract;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return my_title;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getAbstract() {
		return my_abstract;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getFile() {
		return my_file;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getID() {
		return my_id;
	}

	public User getAuthor() {
		return my_author;
	}
	
	public User getSubprogramchair() {
		return my_subprogramchair;
	}
	
	public List<User> getReviewers() {
		return my_reviewers;
	}
	
	public void setSubprogramchair(User the_subprogramchair) {
		my_subprogramchair = the_subprogramchair;
	}
	
	public void addReviewer(User the_reviewer) {
		my_reviewers.add(the_reviewer);
	}
	
	public String getRole() {
		return my_role;
	}
	
	public void setRole(String the_role) {
		my_role = the_role;
	}
	
}