/*
 * 
 */

package model;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import controller.DBConnector;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 *
 */
public class Conference {

	private int my_id;
	
	private String my_date;

	private String my_paperdeadline;
	
	private String my_name;

	private String my_description;
	
	private String my_location;
	
	private List<Paper> my_papers;
	
	private User my_programchair;
	
	/**
	 * 
	 * @param the_id
	 * @param the_name
	 * @param the_description
	 * @param the_date
	 * @param the_paperdeadline
	 * @param the_location
	 */
	public Conference(int the_id, String the_name, String the_description, String the_date, String the_paperdeadline, String the_location, User the_programchair) {
		my_id = the_id;
		my_name = the_name;
		my_description = the_description;
		my_date = the_date;
		my_paperdeadline = the_paperdeadline;
		my_location = the_location;

		my_programchair = the_programchair;
	}
	
	/**
	 * 
	 * @param the_name
	 * @param the_description
	 * @param the_date
	 * @param the_paperdeadline
	 * @param the_location
	 */
	public Conference(String the_name, String the_description, String the_date, String the_paperdeadline, String the_location, User the_programchair) {
		my_name = the_name;
		my_description = the_description;
		my_date = the_date;
		my_paperdeadline = the_paperdeadline;
		my_location = the_location;
		my_papers = new ArrayList<Paper>();
		my_programchair = the_programchair;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return my_name;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getID() {
		return my_id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDescription() {
		return my_description;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPaperdeadline() {
		return my_paperdeadline;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDate() {
		return my_date;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getLocation() {
		return my_location;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Paper> getPapers() {
    return my_papers;
  }
	
	/**
	 * 
	 * @param the_location
	 */
	public void setLocation(String the_location) {
		my_location = the_location;
	}
	
	/**
	 * 
	 * @param the_id
	 */
	public void setID(int the_id) {
	  my_id = the_id;
	}
	
	public User getProgramchair() {
		return my_programchair;
	}


	
}
