/*
 * 
 */
package model;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 * @author Titus Cheng
 *
 */
public class Recommendation {

  private User my_author;

  private String my_recommendation;

  private int my_id;
  
  private String my_date;

  /**
   * 
   * @param the_author
   * @param the_paper
   * @param the_recommendation
   */
  public Recommendation(int the_id, User the_author, String the_recommendation, String the_date) {
    my_author = the_author;
    my_recommendation = the_recommendation;
    my_id = the_id;
    my_date = the_date;
  }

  /**
   * 
   * @param the_id
   * @param the_author
   * @param the_paper
   * @param the_recommendation
   */
  public Recommendation(User the_author, String the_recommendation) {
    my_author = the_author;
    my_recommendation = the_recommendation;
    my_date = new Date().toString();
  }

  /**
   * 
   * @return
   */
  public int getID() {
    return my_id;
  }
  
  public String getMessage() {
	  return my_recommendation;
  }
  
  public String getDate() {
	  return my_date;
  }
  
  public User getUser() {
	  return my_author;
  }
  
  public void setID(int the_id) {
	  my_id = the_id;
  }

}
