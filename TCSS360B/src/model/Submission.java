/*
 * 
 */

package model;

/**
 * 
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 * @author Titus Cheng
 */
public class Submission {
	
	/* 
	 * mySubmission is a string object that holds the name of submission 
	 */
	private String mySubmission;
	
	
	/*
	 * A constructor method that instantiates a submission object.
	 */
	public Submission(final String theSubmission)
	{
		mySubmission = theSubmission;
	}
	
	/*
	 * name returns the name of the submission;
	 */
	public String name()
	{
		final String myName = mySubmission;
		return myName;
	}
	
	public int length()
	{
		return mySubmission.length();
	}
}
