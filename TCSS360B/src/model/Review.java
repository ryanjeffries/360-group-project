/*
 * 
 */
package model;

import java.io.File;
import java.util.Date;

/**
 * @author Jon Judge
 * @author Ryan Jeffries
 * @author Zach Sogolow
 * @author Titus Cheng
 *
 */
public class Review {

  private User my_user;

  private String my_message;

  private String my_date;

  private File my_file;

  private int my_id;

  /**
   * 
   * @param the_id
   * @param the_user
   * @param the_message
   * @param the_date
   * @param the_file
   */
  public Review(int the_id, User the_user, String the_message, String the_date, File the_file) {
    my_user = the_user;
    my_message = the_message;
    my_date = the_date;
    my_file = the_file;
    my_id = the_id;
  }

  /**
   * 
   * @param the_user
   * @param the_message
   * @param the_file
   */
  public Review(User the_user, String the_message, File the_file) {
    my_user = the_user;
    my_message = the_message;
    my_date = new Date().toString();
    my_file = the_file;
  }

  /**
   * 
   * @param the_id
   */
  public void setID(int the_id) {
    my_id = the_id;
  }

  /**
   * 
   * @return
   */
  public String getDate() {
    return my_date;
  }

  /**
   * 
   * @return
   */
  public String getMessage() {
    return my_message;
  }

  /**
   * 
   * @return
   */
  public User getUser() {
    return my_user;
  }

  /**
   * 
   * @return
   */
  public int getID() {
    return my_id;
  }

  /**
   * 
   */
  public String toString() {
    return my_date + "     " + my_user.getID() + "      " + my_message;
  }
}
